//
//  PlayerStatsViewController.swift
//  NBA_Stats_App
//
//  Created by Aaron Kalsi on 07/12/2017.
//  Copyright © 2017 Aaron Kalsi. All rights reserved.
//

import UIKit
import Social

class PlayerStatsViewController: UIViewController {
    
    var selectedPlayer:Player? //Variabel to store selected player passed from TeamRoster
    var playerStats = StatsList() //variable to call on StatsList call in stats model
    var Stats: [Stat] = []//creates empty array of stat object
    
//LABEL INSTANCE VARIABLE LINKING TO LABELS ON STATS SCENE
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var lblFieldGoalsMade: UILabel!
    @IBOutlet weak var lblTwoPointersMade: UILabel!
    @IBOutlet weak var lblThreePointerMade: UILabel!
    @IBOutlet weak var lblMinutesPlayed: UILabel!
    @IBOutlet weak var lblRebounds: UILabel!
    @IBOutlet weak var lblAssists: UILabel!
    @IBOutlet weak var lblSteals: UILabel!
    @IBOutlet weak var lblBlockedShots: UILabel!
    @IBOutlet weak var lblTurnovers: UILabel!
    @IBOutlet weak var imgPlayerPhoto: UIImageView!
//////////////////////////////////////////////////////////////////////////////////////
    //DISPLAY PLAYER IMAGES
    func getImageDataFromURL()->Data
    {
        var imageData:Data
        
        if (selectedPlayer?.PhotoUrl.contains("https:"))! //Downloads image from uel attribute within selected player object
        {
            // try to get the binary data back from the URL
            imageData = try! Data(contentsOf: (URL(string: (selectedPlayer?.PhotoUrl)!))!)
        }
        else //...otherwise, its a local file, extract it from there.
        {
            // need to get a reference to the application bundle...
            let bundle = Bundle.main
            //... get the full path for the image file using the bundle reference
            let path = bundle.path(forResource: selectedPlayer?.PhotoUrl, ofType: nil)
            // try to get binary data back from the image file
            imageData = try! NSData(contentsOfFile: path!) as Data
        }
        // retun the binary data
        return imageData
    }
//////////////////////////////////////////////////////////////////////////////////////
    //SOCIAL MEDIA
    
    func errorAlertController(errorMessage:String)
    {
        let notLoggedInAC = UIAlertController(title: "", message: errorMessage, preferredStyle: UIAlertControllerStyle.actionSheet) //Creats an error alert controller 
        
        notLoggedInAC.addAction(UIAlertAction(title:"Okay", style: UIAlertActionStyle.default, handler: nil)) // creates and adds alert action to the alert controller
        
        present(notLoggedInAC, animated: true, completion: nil) //displays alert controller
        
    }
    
    
    
    @IBAction func shareOptions (sender:AnyObject)
    {
        let showActions = UIAlertController(title: "", message: "Share on Social Media", preferredStyle: UIAlertControllerStyle.actionSheet) // UI Alert Controller (Popup Options) created with with action sheet style (Linked to the share button on the
        
        //Action 1 that will appear in alert controller (POST TO TWITTER)
        let twitter = UIAlertAction(title: "Twitter", style: UIAlertActionStyle.default)
        { (action) -> Void in
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter) //Checks you are logged into account (isAvaliable checks if a request can be sent to a servive)
            {
                let twitterComposeVC = SLComposeViewController(forServiceType: SLServiceTypeTwitter) //creats a twitter compose view controller where users can create a social media post
                
                twitterComposeVC?.setInitialText("I can't believe \((self.selectedPlayer?.FirstName)!) \((self.selectedPlayer?.LastName)!) has made \((self.Stats.first?.ThreePointersMade)!) three pointers this season so far. He is the best player in the NBA.") //sets the social media message in compose VC
                
                self.present(twitterComposeVC!, animated: true, completion: nil) //Display twitter compose view controller
            }
            else
            {
               self.errorAlertController(errorMessage: "You are not logged into Twitter") //Call on the alert controller with an error message to display
            }
            
        }
        
        //Action 2 that will apprear in alert controller (POST TO FACEBOOK)
        let facebook = UIAlertAction(title: "Facebook", style: UIAlertActionStyle.default)
        { (action) -> Void in
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) //Checks you are logged into account (isAvaliable checks if a request can be sent to a servive)
            {
                let facebookComposeVC = SLComposeViewController(forServiceType: SLServiceTypeFacebook) //creats a facebook compose view controller where users can create a social media post
                
                facebookComposeVC?.setInitialText("I can't believe \((self.selectedPlayer?.FirstName)!) \((self.selectedPlayer?.LastName)!) has made \((self.Stats.first?.ThreePointersMade)!) three pointers this season so far. He is the best player in the NBA.") //sets social media message in compose VC
                
                self.present(facebookComposeVC!, animated: true, completion: nil) //Display Facebook compose view controller
            }
            else
            {
                self.errorAlertController(errorMessage: "You are not logged into Facebook") //Call on the error alert controller with an error message to display
            }
            
        
        }
        
        //Action 3 that will appear in the alert controller (CANCEl OPTION)
        let cancel = UIAlertAction(title: "Close", style: UIAlertActionStyle.cancel)
        { (action) -> Void in
            
        }
        
        //ADDS THE ACTION DEFINED ABOVE TO THE showsActions ALERT CONTROLLER
        showActions.addAction(twitter)
        showActions.addAction(facebook)
        showActions.addAction(cancel)
        
        present(showActions, animated: true, completion:nil) //Displays the alert controller that shows social media shring options
        
        
    }

    override func viewDidLoad() {
        //Sets the configuration/data displayed in all of the labels on the page
        super.viewDidLoad()
        print("Player Recieved in Roster: \(selectedPlayer?.displayPlayerIdOnly())")//debug statement
        
        Stats = (playerStats.getStatsFromWebService(selectedPlayer!.PlayerID)) //Calls getStatsFromWebService using the selected PlayerID
        playerStats.showCreatedList() //collects relevent stats for the selected player
        
        lblPlayerName.text = Stats.first?.Name
        lblFieldGoalsMade.text = "Field Goals Made: \((Stats.first?.FieldGoalsMade)!)"
        lblTwoPointersMade.text = "Two Pointers Made: \((Stats.first?.TwoPointersMade)!)"
        lblThreePointerMade.text = "Three Pointers Made: \((Stats.first?.ThreePointersMade)!)"
        lblMinutesPlayed.text = "Minutes Played: \((Stats.first?.Minutes)!)"
        lblRebounds.text = "Rebounds: \((Stats.first?.Rebounds)!)"
        lblAssists.text = "Assists: \((Stats.first?.Assists)!)"
        lblSteals.text = "Steals: \((Stats.first?.Steals)!)"
        lblBlockedShots.text = "Blocked Shots: \((Stats.first?.TwoPointersMade)!)"
        lblTurnovers.text = "Turnovers: \((Stats.first?.Turnovers)!)"
        imgPlayerPhoto.image = UIImage(data:getImageDataFromURL())
        imgPlayerPhoto.layer.borderColor = UIColor.black.cgColor //Sets border colour araound player image
        imgPlayerPhoto.layer.borderWidth = 4 //Sets border width
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
