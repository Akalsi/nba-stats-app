//
//  TeamModel.swift
//  NBA_Stats_App
//
//  Created by Aaron Kalsi on 30/11/2017.
//  Copyright © 2017 Aaron Kalsi. All rights reserved.
//

import Foundation
import UIKit
import CoreData


class TeamList
{
    static var listOfTeams = [Team]() // local list of team objects
    
    static var teamService:TeamService? //team service ref for delegation
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DATA PERSISTANCE
    static func getBookmarkedTeams()->[Team]
    {
        var bookMarkedTeams:[Team] = [Team]() // Variable for array of bookmarked Team objects to be retrieved and returned
        
        var retrievedTeams:[BookMarkedTeam] // variable to hold the records from the DB
        
        let managedContext = DataBaseHelper.persistentContainer.viewContext // get the (only) managed context from the database helper class
        
        let Request = NSFetchRequest<NSManagedObject>(entityName: "BookMarkedTeam") // create a fetch request that will retireve all the entities from the DB
        
        do
        {
            retrievedTeams = try managedContext.fetch(Request) as! [BookMarkedTeam]
            print ("\(retrievedTeams.count) bookmarked teams retrieved from the Database...")
            
            for eachRetrieved in retrievedTeams // Goes through the retrieved Bookmarked records to pass data to the Team attributes
            {
                let k = eachRetrieved.key
                let c = eachRetrieved.city
                let n = eachRetrieved.name
                bookMarkedTeams.append(Team(k!,c!,n!)!) // create a new Team object and add it to the bookMarkedTeams array for display
            }
        }
        catch
        {
            print ("FAILED to fetch bookmarked teams, reason: \(error)")
            bookMarkedTeams.append(Team("<<No Team ID>>","<<No City Name>>","<<No Team Name>>")!)
        }
        return bookMarkedTeams
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    

    static func getTeamsFromWebService()->[Team]
    {
        let searchURL = "https://api.fantasydata.net/v3/nba/stats/JSON/teams?key=b44171646dca426da4690665bb76707a" // Construct the complete URL endpoint to be called
        print ("Web Service call = \(searchURL)")
        
        teamService = TeamService(searchURL) // Creates a  Web Service object that will make the call - initialiser
        
        let operationQ = OperationQueue() // Create an operation queue - to allow the web service to be called on a second thread
        
        operationQ.maxConcurrentOperationCount = 1 // Only allow one task at a time in queue
        
        operationQ.addOperation(teamService!) // Queue the web service object as the operation object to be run on this thread
        
        operationQ.waitUntilAllOperationsAreFinished() // Wait for the operation to complete [Synchronous] - better to use delegation, for [Asynchronous]
        
        listOfTeams.removeAll()
        
        let returnedJSON = teamService!.jsonFromResponse // Get the raw JSON back from the completed service call [complete dataset]
        
        let JSONObjects = returnedJSON as! [[String:Any]] // Extract the array of teams

        for eachJSONObject in JSONObjects // Go through the returned data objcts in the array
        {
            print("Creating Team object from JSON: \(eachJSONObject)") //Prints the created JSON objects
            listOfTeams.append(Team(eachJSONObject)) // Adds new team object to the main team array
        }
        
        showCreatedList()
        return listOfTeams
    }
    
    static func showCreatedList()
    {
        for team in listOfTeams //iterates through the team objects within the listOfTeams array
        {
            print(team.displayTeam()) // Prints the daiaplay name of the team
            print(team.displayTeamKeyOnly()) // Prints the abbrevation/key of the team that will be used to het th team roster
        }
        
    }
    
}


class Team
{
    //These variables will be called on and assigned to lables within the VC the model is linked to 
    private (set)var Key:String // Variable to store the key of the team
    private (set)var City:String // Variable to store the city of the team
    private (set)var Name:String // Variable to store the name of the team
    private (set)var isBookmarked:Bool // Bool variable to check if a team has been bookmarked or not
    
    
    init?(_ k:String, _ c:String, _ n:String)
    {
        //Check to ensure all fields are present to create team objct, if not then fails
        if ((k == "") || (c == "") || (n == ""))
        {
            return nil
        }
        else
        {
            Key = k
            City = c
            Name = n
            isBookmarked = false
        }
    }
    convenience init(_ JSONObject:[String:Any])
    {   // extract attributres from each of the Key:Value pairs in the JSON object
        let key = JSONObject["Key"]
        let city = JSONObject["City"]
        let name = JSONObject["Name"]
        
        self.init(key! as! String,city! as! String,name! as! String)! // Call the initialiser to setup a team object based on the values.
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DATA PERSISTANCE
    func BookMark(_ save:Bool)
    {
        // get the (only) managed context...
        let managedContext = DataBaseHelper.persistentContainer.viewContext
       
        let entity = NSEntityDescription.entity(forEntityName: "BookMarkedTeam", in: managedContext)! //get the table description of the entity [BookMarkedTeam]
        
        let bookMarkedTeam = NSManagedObject(entity: entity, insertInto: managedContext) //retrieve the associated Maanaged Object for the BookMarkedTeam entity
        // put Team object members into matching attributes in the Managed Object
        bookMarkedTeam.setValue(self.Key, forKeyPath: "key")
        bookMarkedTeam.setValue(self.City, forKeyPath: "city")
        bookMarkedTeam.setValue(self.Name, forKeyPath: "name")
        
        
        if (save)
        {
            do
            {
                try managedContext.save() //Commits any unsaved changes
                isBookmarked = true //bookmark bool to true
                print("Team: \(City) \(Name) was ADDED to the Bookmarks Database!")
            }
            catch let error as NSError //Catches the save error
            {
                print("Could not save \(City) \(Name) . Reason: \(error), \(error.userInfo)") //prints reason for error
            }
        }
        else
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "BookMarkedTeam") // create a fetchrequest to associate with the BookMarkedTeam entity
            fetchRequest.predicate = NSPredicate(format: "key == %@", Key) // set a query to select a unique record based on team key
            
            do
            {
                // run the query to get the resultset (can be more than one record)
                let resultData = try managedContext.fetch(fetchRequest) as! [BookMarkedTeam] //gets array of objects that meet the fetch request
                print("Found \(resultData.count) records to delete...")
                // iterate through all the found results
                for object in resultData
                {
                    managedContext.delete(object) //removes onject within resultData
                    print("Team: \(City) \(Name) was REMOVED from Bookmarks Database!")
                }
                try! managedContext.save() //Commits any unsaved changes to complete deletion
                self.isBookmarked = false //set bookmark bool to fale
            }
            catch //catches error
            {
                print("Could not find any records to delete...")
            }
        }
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Debug Messages
    func displayTeam()->String // Function to display the name of the teams
    {
        let strTeam = "The Team has name \(City) \(Name)"
        return strTeam
    }
    func displayTeamKeyOnly() -> String //Function to display the key associated with the teams
    {
        let strkey = "The team has Key \(Key)"
        return strkey
    }
    //
}

protocol TeamServiceDelegate //Protocol only requires one method to be implemented
{
    func serviceFinished(_ service:TeamService, _ error:Bool)
}

class TeamService:Operation
{
    var urlRecieved: URL? //URL object to call the service
    
    var jsonFromResponse: [Any]? //creats JSON object as response
    
    var delegate:TeamServiceDelegate? //Assigns delegate as attribute of class
    
    init(_ incomingURLString:String)
    {
        urlRecieved = URL(string: incomingURLString.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!) //constructor to allow URL string to be converterted into URL object (adding PercentEncoding to deal with spaces (etc) in the URL String)
    }
    
    override func main()
    {
        var responseData:Data?
        
        // call the web service to get the data object
        do
        {
            responseData = try Data(contentsOf: urlRecieved!) //Gets data From service call
            print("Service call (request) succesful! Returned: \(responseData!)") //prints response data
        }
        catch //Catch exception
        {
            print("Service call (request) failed!!")
        }
        
        // parse the overall JSON from the returned data object
        do
        {
            jsonFromResponse = try JSONSerialization.jsonObject(with: responseData!, options: .allowFragments) as? [Any] //Convert response data to JSON
            print("JSON Parser succesful! Returned: \(jsonFromResponse!)") //Prints returned JSON data
            delegate?.serviceFinished(self, true)
        }
        catch //Catch Exception
        {
            print("JSON Parser failed!!")
            delegate?.serviceFinished(self, false)
        }
    }
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DATA PERSISTANCE
class DataBaseHelper
{
    // MARK: - Core Data stack
    // make the persistentContainer a class attribute - so it can be used to access the managed context
    static var persistentContainer: NSPersistentContainer =
        {
            let container = NSPersistentContainer(name: "NBA_Stats_App") //Sets and creates data persistance container
            
            container.loadPersistentStores(completionHandler: { (storeDescription, error) in
                if let error = error as NSError? {
                    fatalError("Unresolved error \(error), \(error.userInfo)") //Checks if there is an error and prints error
                }
            })
            return container
    }()
    
    // MARK: - Core Data Saving support
    
    static func saveContext () {
        let context = persistentContainer.viewContext //View context of persistance container
        
        //Checks if content of the container has changed if so new changes are saved, in additon exception is caught.
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
}


