//
//  SelectTeamTableViewController.swift
//  NBA_Stats_App
//
//  Created by Aaron Kalsi on 16/11/2017.
//  Copyright © 2017 Aaron Kalsi. All rights reserved.
//

import UIKit

class SelectTeamTableViewController: UITableViewController
{
    
    let reuseIdentifier = "TeamTableViewCell"

    let Teams:[Team] = TeamList.getTeamsFromWebService() //Varible to hold teams when called on from service (API CALL)

//////////////////////////////////////////////////////////////////////////////////////
    //DATA Persistance ADD to BOOKMARKS
    var LastPressTime = Date() //variale to track last press made on the page
    
    func BookMarkPress (_ sender: UILongPressGestureRecognizer) //funcation to add team to bookmark
    {
        if Date().timeIntervalSince(LastPressTime) > 3 // check if it's been at least 3 seconds since the last time of long press
        {
            LastPressTime = Date() //reset the time
            let Touch = sender.location(in: self.view) //Get the point that is pressed on screen with long press
            if let indexPath = self.tableView.indexPathForRow(at: Touch) // check if row being pressed if so index of the row is returned
            {
                if !(Teams[indexPath.row].isBookmarked) //checks that team is NOT bookmarked
                {
                    print("Selected item: \(Teams[indexPath.row].displayTeam())") //prints the selected item
                    self.tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.blue //Changes BG colour of cell blue
                    Teams[indexPath.row].BookMark(true) //adds to bookmarked teams
                    return
                }
            }
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //Creates and Registerd long press
        let LongPressRecog = UILongPressGestureRecognizer(target: self, action: #selector(SelectTeamTableViewController.BookMarkPress(_:))) //creates long press recogniser
        tableView.addGestureRecognizer(LongPressRecog) //registers/adds longpressrecog to the the table view
        
        self.clearsSelectionOnViewWillAppear = false

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        for Count in 0...Teams.count-1
        {
            if (Teams[Count].isBookmarked) //Checks if a team is bookmarked and if so chnages BG colour to blue and if not white
            {
                self.tableView.cellForRow(at: IndexPath(row: Count, section: 0))?.backgroundColor = UIColor.blue
                //Teams.remove(at: Count)
            }
                
            else
                
            {
                self.tableView.cellForRow(at: IndexPath(row: Count, section: 0))?.backgroundColor = UIColor.white
            }
        }
    self.tableView.reloadData()
    }
//////////////////////////////////////////////////////////////////////////////////////
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1 //number of prototype cells
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection //Generates number of cells equal to the number of teams within team array
        section: Int) -> Int {
        return Teams.count //returns the number of elements within the team array
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! TeamTableViewCell //sets reuse identifier for cells equal to the number of teams within the teams list

        cell.lblTeamName.text = Teams[indexPath.row].City + " " + Teams[indexPath.row].Name // Team name displayed within the cell
        return cell
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if (segue.identifier == "TeamRosterSegue") //Checks segue identifier
        {
            let destVC1 = segue.destination as! TeamRosterTableViewController
        
            // item that was selected in tableview
            let selectedTeamIndex = self.tableView.indexPathForSelectedRow?.row //Selects team objct for the specific row that has been selected
            destVC1.selectedTeam = Teams[selectedTeamIndex!] //Passes Team obj Information onto the TeamRosterViewController
        }
        else if (segue.identifier == "BookMarkTeamsSegue") //Checks segue identifier
        {
            let destVC2 = segue.destination as! BookMarkedTeamsTableViewController
            let selectedTeamIndex2 = self.tableView.indexPathForSelectedRow?.row //Selects team object for the specific row that has been selected
            destVC2.selectedTeam = Teams[selectedTeamIndex2!] //Passes the team objc onto the BookMarkedTeamsTableVC
        }
    
    }
  

}
