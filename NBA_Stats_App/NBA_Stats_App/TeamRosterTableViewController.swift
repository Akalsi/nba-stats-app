//
//  TeamRosterTableViewController.swift
//  NBA_Stats_App
//
//  Created by Aaron Kalsi on 20/11/2017.
//  Copyright © 2017 Aaron Kalsi. All rights reserved.
//

import UIKit

class TeamRosterTableViewController: UITableViewController {

    var selectedTeam:Team? //Variable to store selected team from select team VC
    var teamRoster = PlayerList() //Variable to call on the playerlist class within player model
    var Players: [Player] = []//Creates empty array of player objects
    
    let reuseIdentifier = "PlayerTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        print("Team Recieved in Roster: \(selectedTeam?.displayTeamKeyOnly())") //Debug statement
        Players = (teamRoster.getPlayersFromWebService(selectedTeam!.Key)) //players stored within array Players of type Player and uses the selected team key to get players for team (API CALL)
        teamRoster.showCreatedList() //calls on showcreatedlist() function within PlayerList class in player model.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1 //numer of prototype cells
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection
        section: Int) -> Int {
        return (Players.count) //Returns number of player within array
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! PlayerTableViewCell //creats the correct amount of cells that are equal to the number of players in Players array
        
        // Configure the cell...
        cell.lblPlayerName.text = (Players[indexPath.row].FirstName + " " + Players[indexPath.row].LastName) //Configure cells to display player names
        return cell
    }
    
    // Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if (segue.identifier == "PlayerStatsSegue") //if player is selected then pass player object onto playerstatsviewcontroller
        {
            let destVC1 = segue.destination as! PlayerStatsViewController
            
            let selectedPlayerIndex = self.tableView.indexPathForSelectedRow?.row // item that was selected in tableview
            destVC1.selectedPlayer = Players[selectedPlayerIndex!] //passed the player that is select into selectedPlaver variable in player stats VC
        }
        else if (segue.identifier == "WebSearchSegue") //If web browser is selected pass on the recieved team object onto the WebBrowserViewController
        {
            let destVC2 = segue.destination as! WebBrowserViewController
            
            let webTeam = self.selectedTeam!
            print("webTeam: \(webTeam)")
            destVC2.recievedTeam = webTeam //Passes selected team object onto recieved team variable in WebBrowserViewController
        }
        
    }
    

}
