//
//  StatsModel.swift
//  NBA_Stats_App
//
//  Created by Aaron Kalsi on 07/12/2017.
//  Copyright © 2017 Aaron Kalsi. All rights reserved.
//

import UIKit

import Foundation
import UIKit
import CoreData

class StatsList
{
    static var listOfStats = [Stat]() //Locally maintained list of stats object
    
    static var statService:StatService? //Ref to StatService for delegation
    
    func getStatsFromWebService(_ PlayerID:Int)->[Stat]
    {
        let searchURL = "https://api.fantasydata.net/v3/nba/stats/JSON/PlayerSeasonStatsByPlayer/2018/\(PlayerID)?key=b44171646dca426da4690665bb76707a" // construct the complete URL endpoint to be called using the selectedPlayer.PlayerID (segue player attribute passed from selectPlayerVC) as the PlayerID
        
        print ("Web Service call = \(searchURL)") //debug statement to print URL
        
        StatsList.statService = StatService(searchURL) //Creates a web service object to make the call
        
        let operationQ = OperationQueue() // Creates an operation queue - to allow the web service to be called on a second thread

        operationQ.maxConcurrentOperationCount = 1 //Allows one task at a time in queue

        operationQ.addOperation(StatsList.statService!) //Adds the created web service object as the operation object to rin in the queue
        
        operationQ.waitUntilAllOperationsAreFinished() //Waits for the operation to complete [Synchronous] - better to use delegation, for [Asynchronous]
        
        StatsList.listOfStats.removeAll() // Clear the current list of stats
    
        let returnedJSON = StatsList.statService!.jsonFromResponse //Get the raw JSON back from the completed service call

        let JSONObjects = [returnedJSON!] as [[String:Any]] //Extract the array of stats for the specific player
        
        for eachJSONObject in JSONObjects // Go through the returned data objects in the array
        {
            print("Creating player stats object from JSON: \(eachJSONObject)") //Prints the json data for each object
            StatsList.listOfStats.append(Stat(eachJSONObject)) //Adds the new stat object to the main stats array
        }
        
        return StatsList.listOfStats // Return list of Stat objects to UI
    }
    
    func showCreatedList() //Function to display the debug message of the stats for a soecific player
    {
        for stat in StatsList.listOfStats
        {
            print(stat.displayPlayerStats()) //displays the stats of a player
            
        }
        
    }
    
    init(){
        
    }
}


class Stat
{
    //These variables will be called on and assigned to lables within the VC the model is linked to 
    private (set)var Name:String //variable to store the name of a player
    private (set)var FieldGoalsMade:Float //variable to store the FieldGoalsMade by a player
    private (set)var TwoPointersMade:Float //variable to store the TwoPointersMade by a player
    private (set)var ThreePointersMade:Float //variable to store the ThrePointersMade by a player
    private (set)var Minutes:Int //variable to store the Minuits played by a player
    private (set)var Rebounds:Float //variable to store the number of rebounds by a player
    private (set)var Assists:Float //variable to store the number of assists by a player
    private (set)var Steals:Float //variable to store the number of steals made by a player
    private (set)var BlockedShots:Float //variable to store the number of BlockedShots by a player
    private (set)var Turnovers:Float //variable to store the number of Turnovers by a player
    
    
    
    init?(_ Nam:String, _ fgm:Float, _ Twopm:Float, _ Threepm:Float, _ mins:Int, _ reb:Float, _ ast:Float, _ steals:Float, _ blksht:Float, _ turovr:Float)
    {
        //Checks to ensure that all fields are present and valid to create the stat object, if not then this fails and nil is returned
        if ((Nam == "") || (fgm < 0.0) || (Twopm < 0.0) || (Threepm < 0.0) || (mins < 0) || (reb < 0.0) || (ast < 0.0) || (steals < 0.0) || (blksht < 0.0) || (turovr < 0.0))
        {
            return nil
        }
        else
        {
            Name = Nam
            FieldGoalsMade = fgm
            TwoPointersMade = Twopm
            ThreePointersMade = Threepm
            Minutes = mins
            Rebounds = reb
            Assists = ast
            Steals = steals
            BlockedShots = blksht
            Turnovers = turovr
        }
    }
    
    convenience init(_ JSONObject:[String:Any])
    {   // extract attributres from each of the Key:Value pairs in the JSON object
        let name = JSONObject["Name"]
        let fieldgoalsmade = JSONObject["FieldGoalsMade"]
        let twopointersmade = JSONObject["TwoPointersMade"]
        let threepointersmade = JSONObject["ThreePointersMade"]
        let minutes = JSONObject["Minutes"]
        let rebounds = JSONObject["Rebounds"]
        let assists = JSONObject["Assists"]
        let steals = JSONObject["Steals"]
        let blockedshots = JSONObject["BlockedShots"]
        let turnovers = JSONObject["Turnovers"]

        self.init(name! as! String,fieldgoalsmade! as! Float,twopointersmade! as! Float,threepointersmade! as! Float,minutes! as! Int,rebounds! as! Float,assists! as! Float,steals! as! Float,blockedshots! as! Float,turnovers! as! Float)! // Call the initialiser to setup a stat object based on the values.
    }

    //Debug Messages
    func displayPlayerStats()->String //Debug function that shows the stats for the player
    {
        let strPlayerStats = "The Player has stats name \(Name), FGM \(FieldGoalsMade), 2PS \(TwoPointersMade), 3PS \(ThreePointersMade), mins \(Minutes), reb \(Rebounds), Assists \(Assists), stl \(Steals), blksht \(BlockedShots), turovrs \(Turnovers)"
        return strPlayerStats
    }
    //
}

protocol StatServiceDelegate //Protocol only requires one method to be implemented
{
    func serviceFinished(_ service:StatService, _ error:Bool)
}

class StatService:Operation
{
    var urlRecieved: URL? //The class recieves a URL object to call the service
    
    var jsonFromResponse: [String:Any]? //Creates JSON object as response 
    
    var delegate:StatServiceDelegate? //Assigns delegate as attribute of class
    
    init(_ incomingURLString:String)
    {
        urlRecieved = URL(string: incomingURLString.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!) //Constructor to allow URL string to be converterted into URL object (adding PercentEncoding to deal with spaces in the URL String)
    }
    
    override func main()
    {
        var responseData:Data?
        
        // call the web service to get the data object
        do
        {
            responseData = try Data(contentsOf: urlRecieved!) //Gets data From service call
            print("Service call (request) succesful! Returned: \(responseData!)") //Prints response data
        }
        catch //Exception Caught
        {
            print("Service call (request) failed!!")
        }
        
        // parse the overall JSON from the returned data object
        do
        {
            jsonFromResponse = try JSONSerialization.jsonObject(with: responseData!, options: .allowFragments) as? [String:Any] //Converts Response Data to JSON
            print("JSON Parser succesful! Returned: \(jsonFromResponse!)") //Prints returned JSON data 
            delegate?.serviceFinished(self, true)
        }
        catch //Exception Caught
        {
            print("JSON Parser failed!!")
            delegate?.serviceFinished(self, false)
        }
    }
    
}

