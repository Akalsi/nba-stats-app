//
//  PlayerModel.swift
//  NBA_Stats_App
//
//  Created by Aaron Kalsi on 07/12/2017.
//  Copyright © 2017 Aaron Kalsi. All rights reserved.
//

import UIKit

import Foundation
import UIKit
import CoreData

class PlayerList
{
    static var listOfPlayers = [Player]() //Local list of player objects
    
    static var playerService:PlayerService? //Player service ref for delegation
    
    func getPlayersFromWebService(_ teamKey:String)->[Player]
    {
        let searchURL = "https://api.fantasydata.net/v3/nba/stats/JSON/Players/\(teamKey)?key=b44171646dca426da4690665bb76707a" // construct the complete URL endpoint to be called using the selectedTeam.Key (segue team attribute passed from selectTeamVC) as the teamKey
        
        print ("Web Service call = \(searchURL)") //debug statement to print the URL
        
        PlayerList.playerService = PlayerService(searchURL) // creates a  Web Service object that will make the call - initialiser
        
        let operationQ = OperationQueue() // create an operation queue - to allow the web service to be called on a second thread
        
        operationQ.maxConcurrentOperationCount = 1 //Allow one task at a time in the queue
        
        operationQ.addOperation(PlayerList.playerService!) //Adds web service object to the operation queue
        
        operationQ.waitUntilAllOperationsAreFinished() //// Wait for the operation to complete
        
        PlayerList.listOfPlayers.removeAll()
        
        let returnedJSON = PlayerList.playerService!.jsonFromResponse //Get the raw JSON back from the completed service call
        
        let JSONObjects = returnedJSON as! [[String:Any]] //Extract the array of players
        
        for eachJSONObject in JSONObjects //Go through the returned objects in the array
        {
            print("Creating Player object from JSON: \(eachJSONObject)") //Prints the json data for each object
            PlayerList.listOfPlayers.append(Player(eachJSONObject)) //Adds the new player object to the main array of players
        }
        //Return the list of player objects to the UI
        showCreatedList()
        return PlayerList.listOfPlayers
    }
    
    func showCreatedList()
    {
        for player in PlayerList.listOfPlayers
        {
            print(player.displayPlayer()) //Prints the name of the player
            print(player.displayPlayerIdOnly()) //Prints the player ID associated wih the player
            print(player.displayPlayerPhoto()) //Prints the photo URL associated wih the player
        }
        
    }
    
    init(){
        
    }
}


class Player
{
    //These variables will be called on and assigned to lables within the VC the model is linked to 
    private (set)var PlayerID:Int //Variable to store the player ID of the player
    private (set)var FirstName:String //Variable to store the first name of the player
    private (set)var LastName:String //Variable to store the last name of the player
    private (set)var PhotoUrl:String //Variable to store the photo URL associated with the player
    
    
    
    init?(_ pid:Int, _ fn:String, _ ln:String, _ img:String)
    {
        
        //Check to ensure all fields are present to create team object, if not then fails
        if ((pid <= 0) || (fn == "") || (ln == "") || (img == ""))
        {
            return nil
        }
        else
        {
            PlayerID = pid
            FirstName = fn
            LastName = ln
            PhotoUrl = img
        }
    }
    
    convenience init(_ JSONObject:[String:Any])
    {   // extract attributres from each of the Key:Value pairs in the JSON object
        let playerid = JSONObject["PlayerID"]
        let firstname = JSONObject["FirstName"]
        let lastname = JSONObject["LastName"]
        let photourl = JSONObject["PhotoUrl"]
        
        self.init(playerid! as! Int,firstname! as! String,lastname! as! String, photourl as! String)! // Call the initialiser to setup a player object based on the values.
    }

    //Debug Messages
    func displayPlayer()->String //Function to display the player FirstName and LastName
    {
        let strPlayer = "The Player has name \(FirstName) \(LastName)"
        return strPlayer
    }
    func displayPlayerIdOnly() -> String //Function that shows the PlayerID associated with the player
    {
        let strID = "The Player has ID \(PlayerID)"
        return strID
    }
    func displayPlayerPhoto() -> String //Function that shows the PhotoURL associated with the player
    {
        let strPhoto = "The Player has image \(PhotoUrl)"
        return strPhoto
    }
    //
    
}

protocol PlayerServiceDelegate //Protocol only requires one method to be implemented
{
    func serviceFinished(_ service:PlayerService, _ error:Bool)
}

class PlayerService:Operation
{
    var urlRecieved: URL? //The class recieves a URL object to call the service
   
    var jsonFromResponse: [Any]? //Creats a json objct as response
    
    var delegate:PlayerServiceDelegate? //Assigns delegate as attribute of class

    init(_ incomingURLString:String)
    {
        urlRecieved = URL(string: incomingURLString.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!) //constructor to allow URL string to be converterted into URL object (adding PercentEncoding to deal with spaces in the URL String)
    }
    
    override func main()
    {
        var responseData:Data?
        
        // call the web service to get the data object
        do
        {
            responseData = try Data(contentsOf: urlRecieved!) //Gets data From service call
            print("Service call (request) succesful! Returned: \(responseData!)") //Prints response data
        }
        catch //Caught Exception
        {
            print("Service call (request) failed!!")
        }
        
        // parse the overall JSON from the returned data object
        do
        {
            jsonFromResponse = try JSONSerialization.jsonObject(with: responseData!, options: .allowFragments) as? [Any] //Convert response data to JSON
            print("JSON Parser succesful! Returned: \(jsonFromResponse!)") //Prints returned JSON data 
            delegate?.serviceFinished(self, true)
        }
        catch //Catch error
        {
            print("JSON Parser failed!!")
            delegate?.serviceFinished(self, false)
        }
    }
    
}
