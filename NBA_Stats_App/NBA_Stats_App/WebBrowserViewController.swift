//
//  WebBrowserViewController.swift
//  NBA_Stats_App
//
//  Created by Aaron Kalsi on 14/12/2017.
//  Copyright © 2017 Aaron Kalsi. All rights reserved.
//

import UIKit
import WebKit

class WebBrowserViewController: UIViewController, WKNavigationDelegate, WKUIDelegate
{
    var recievedTeam:Team? // Variable to
    var webView: WKWebView!
    
    
    override func loadView()
    {
        let webConfig = WKWebViewConfiguration() //Variable that calls on the WKWebview configuration class within the WebKit framework
        webView = WKWebView(frame: .zero, configuration: webConfig) //creats a webview with frame zero and uses web config configuration
        webView.uiDelegate = self
        view = webView
        
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let searchTerm:String = "\((self.recievedTeam?.City)!) \((self.recievedTeam?.Name)!)" //passes the city and name of the selected team as the search term for the web search.
        print("SearchTerm: \(searchTerm)") //prints the search term
        let myURL = URL(string: "https://www.google.com/search?q=\(searchTerm)".addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!) //sets the URL to use within the web browser
        
        let myRequest = URLRequest(url: myURL!) //Converts the URL into a URL Request
        webView.load(myRequest) //Loads the Webview with the request URL
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
