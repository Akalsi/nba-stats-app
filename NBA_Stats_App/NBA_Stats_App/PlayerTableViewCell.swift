//
//  PlayerTableViewCell.swift
//  NBA_Stats_App
//
//  Created by Aaron Kalsi on 07/12/2017.
//  Copyright © 2017 Aaron Kalsi. All rights reserved.
//

import UIKit

class PlayerTableViewCell: UITableViewCell
{
    
    @IBOutlet weak var lblPlayerName: UILabel!

}
