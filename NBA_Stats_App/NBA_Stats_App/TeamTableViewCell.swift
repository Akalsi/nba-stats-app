//
//  TeamTableViewCell.swift
//  NBA_Stats_App
//
//  Created by Aaron Kalsi on 04/12/2017.
//  Copyright © 2017 Aaron Kalsi. All rights reserved.
//

import UIKit

class TeamTableViewCell: UITableViewCell
{
    @IBOutlet weak var lblTeamName: UILabel!

}
