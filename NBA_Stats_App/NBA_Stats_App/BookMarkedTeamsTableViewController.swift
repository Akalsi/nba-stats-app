//
//  BookMarkedTeamsTableViewController.swift
//  NBA_Stats_App
//
//  Created by Aaron Kalsi on 13/12/2017.
//  Copyright © 2017 Aaron Kalsi. All rights reserved.
//

import UIKit
import Foundation

private let reuseIdentifier = "BookMarkedTeamTableViewCell"
////////////////////////////////////////////////////////////////////////////////////////////
//DATA PERSISTANCE LONG PRESS TO DELETE BOOKMARK
class BookMarkedTeamsTableViewController: UITableViewController {
    var selectedTeam:Team? //varible to store selected team passed on from the TeamRosterVC
    var bookMarkedTeams = TeamList.getBookmarkedTeams() //Calls on getbookmarkedteams function within TeamList class in TeamModel

    override func viewDidLoad() {
        super.viewDidLoad()
        //Creates and register LPR
        let longPressRecog = UILongPressGestureRecognizer(target: self , action: #selector(UnBookMarkPress(_:)))
        tableView?.addGestureRecognizer(longPressRecog)
    }
    
    var LPTime = Date() //Varible to stores/recored time passed between long presses
    
    func UnBookMarkPress(_ sender: UILongPressGestureRecognizer)
    {
        if Date().timeIntervalSince(LPTime) > 3 // check if it's been at least 3 seconds since the last time of long press
        {
            LPTime = Date() // reset the time
            let touchPoint = sender.location(in: self.view) //get the point that is pressed on screen with long press
            // ... so that the indexpath for the item can be retrieved
            if let indexPath = tableView?.indexPathForRow(at: touchPoint) //checks if touch point is in the table if so the index path is retrieved
            {
                print("Selected item: \(bookMarkedTeams[indexPath.item].displayTeam())") //prints selected team
                bookMarkedTeams[indexPath.row].BookMark(false) // clear the Bookmark for the selected Team - i.e. delete the object from Bookmarks DB
                bookMarkedTeams = TeamList.getBookmarkedTeams() //Updates the bookmarked teams array
                self.tableView?.reloadData() //Updates the VC
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        bookMarkedTeams = TeamList.getBookmarkedTeams()
        print("Teams Bookmarked = \(bookMarkedTeams)") // prints the bookmarked teams
        self.tableView?.reloadData() // Updates VC
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1 //Number of prototype cells
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { 
        return bookMarkedTeams.count //Returns number of player within array
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! BookMarkedTeamTableViewCell //creats the correct amount of cells that are equal to the number of teams in the bookmarked teams array

        // Configure the cell...
        cell.lblBookMarkedTeam.text = bookMarkedTeams[indexPath.row].City + " " + bookMarkedTeams[indexPath.row].Name // Configure cells to to display bookmarked teams.
        return cell
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destVC = segue.destination as! TeamRosterTableViewController
        let selectedTeamIndex = self.tableView?.indexPathForSelectedRow?.row
        destVC.selectedTeam = bookMarkedTeams[selectedTeamIndex!] //Passes the selected team informaltio onto the Team Roster VC.
    }

}
